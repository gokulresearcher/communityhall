<?php

declare(strict_types=1);

namespace App\Application\Query;


use App\Domain\Booking\ValueObject\BookingId;

class GetBookingRequest
{
    private BookingId $bookingId;

    public function __construct(BookingId $bookingId)
    {
        $this->bookingId = $bookingId;
    }

    public function getBookingId(): BookingId
    {
        return $this->bookingId;
    }
}