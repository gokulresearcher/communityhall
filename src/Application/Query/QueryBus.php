<?php

declare(strict_types=1);

namespace App\Application\Query;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class QueryBus
{
    use HandleTrait;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @param object|Envelope $query
     *
     * @return mixed The handler returned value
     *
     * @throws \Throwable
     */
    public function query($query)
    {
        try{
            return $this->handle($query);
        } catch (HandlerFailedException $exception) {
            while ($exception instanceof HandlerFailedException) {
                /** @var \Throwable $exception */
                $exception = $exception->getPrevious();
            }

            throw $exception;
        }
    }
}