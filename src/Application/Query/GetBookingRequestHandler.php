<?php

declare(strict_types=1);

namespace App\Application\Query;


use App\Infrastructure\Services\FetchBooking;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetBookingRequestHandler implements MessageHandlerInterface
{
    private FetchBooking $fetchBooking;

    public function __construct(FetchBooking $fetchBooking)
    {
        $this->fetchBooking = $fetchBooking;
    }

    public function __invoke(GetBookingRequest $getBookingRequest)
    {
        return $this->fetchBooking->getBooking($getBookingRequest->getBookingId());
    }
}