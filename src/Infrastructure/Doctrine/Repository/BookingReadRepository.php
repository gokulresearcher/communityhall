<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Booking\Booking;
use App\Domain\Booking\BookingNotFound;
use App\Domain\Booking\Bookings;
use App\Domain\Booking\ShowBooking;
use App\Domain\Booking\ValueObject\BookingId;
use App\Domain\Booking\ValueObject\BookingStatus;
use Doctrine\ORM\EntityManagerInterface;
use \DateTimeImmutable;
use App\Infrastructure\Doctrine\Entity\Booking as BookingEntity;
use Doctrine\Persistence\ObjectRepository;

class BookingReadRepository implements ShowBooking
{
    private ObjectRepository $entityRepository;

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $entityManager->getRepository(BookingEntity::class);
    }

    public function findBookings(int $forYear): Bookings
    {
        $bookings = [];
        $entityCollection = $this->entityRepository->findAll();

        /** @var BookingEntity $bookingEntity */
        foreach ($entityCollection as $bookingEntity) {
            $bookings[] = $bookingEntity->toDomain();
        }

        return  Bookings::fromArray($bookings);
    }

    public function findBookingById(BookingId $bookingId): Booking
    {
        $where = ['id' => $bookingId->getId()];
        /** @var BookingEntity $bookingEntity */
        $bookingEntity = $this->entityRepository->findOneBy($where);

        if  (!$bookingEntity) {
            throw BookingNotFound::becauseTheBookingIdDoesNotExists($bookingId);
        }

        return $bookingEntity->toDomain();
    }

    public function checkAvailability(DateTimeImmutable $bookingDate): BookingStatus
    {
        $where = [
            'startDate' => $bookingDate,
            'OR endtDate' => $bookingDate,
        ];
        /** @var BookingEntity $bookingEntity */
        $bookingEntity = $this->entityRepository->findBy($where);

        $status = BookingStatus::available();
        if  ($bookingEntity) {
            $status = BookingStatus::from($bookingEntity->getStatus());
        }

        return $status;
    }
}