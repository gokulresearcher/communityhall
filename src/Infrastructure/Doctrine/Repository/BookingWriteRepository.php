<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Booking\Booking;
use App\Domain\Booking\CancelBooking;
use App\Domain\Booking\SaveBooking;
use App\Domain\Booking\ValueObject\BookingId;
use Doctrine\ORM\EntityManagerInterface;
use App\Infrastructure\Doctrine\Entity\Booking as BookingEntity;
use Doctrine\Persistence\ObjectRepository;

class BookingWriteRepository implements SaveBooking, CancelBooking
{
    private ObjectRepository $entityRepository;

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $entityManager->getRepository(BookingEntity::class);
    }

    public function saveBooking(Booking $booking): BookingId
    {
        // TODO: Implement saveBookingDetails() method.
    }

    public function cancelBooking(BookingId $bookingId): void
    {
        // TODO: Implement removeReservation() method.
    }
}