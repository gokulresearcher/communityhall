<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Infrastructure\Doctrine\Entity\Booking;

/**
 * @ORM\Entity
 * @ORM\Table(name="payment")
 */
class Payment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * One Booking has One Payment.
     * This is just for now, in future we can have 1-to-many
     *
     * @ORM\OneToOne(targetEntity="Booking")
     * @ORM\JoinColumn(name="booking_id", referencedColumnName="id")
     */
    private $bookingId;

    /**
     * @ORM\Column(type="float")
     */
    private $amountPaidForHouseCleaning;

    /**
     * @ORM\Column(type="float")
     */
    private $amountPending;

    /**
     * @ORM\Column(type="float")
     */
    private $amountPaid;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $amountPaidDate;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('cash', 'googlepay')")
     */
    private $amountPaidMode;

    /**
     * @ORM\Column(type="float")
     */
    private $amountRefund;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $amountRefundedDate;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('cash', 'googlepay')")
     */
    private $amountRefundMode;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedDate;
}