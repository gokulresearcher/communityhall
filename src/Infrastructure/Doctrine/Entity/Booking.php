<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Entity;

use App\Domain\Booking\ValueObject\BookingAuditInformation;
use App\Domain\Booking\ValueObject\BookingId;
use App\Domain\Booking\ValueObject\BookingInformation;
use App\Domain\Booking\ValueObject\BookingStatus;
use App\Domain\Booking\ValueObject\BookingType;
use App\Domain\Booking\ValueObject\BookingUser;
use App\Domain\Payment\ValueObject\PaymentStatus;
use Doctrine\ORM\Mapping as ORM;
use App\Infrastructure\Doctrine\Entity\Payment;
use App\Domain\Booking\Booking as BookingDomain;
use \DateTimeImmutable;

/**
 * @ORM\Entity
 * @ORM\Table(name="booking")
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $flatNumber;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('hall', 'chair', 'table')")
     */
    private $type;

    /**
     * @ORM\Column(type="text")
     */
    private $reason;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @ORM\Column(type="smallint")
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('pending', 'approved')")
     */
    private $status;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('pending', 'approved')")
     */
    private $paymentStatus;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $feedback;

    public function getStatus(): string
    {
        return $this->status;
    }
    public function toDomain(): BookingDomain
    {
        return BookingDomain::populateData(
            BookingUser::from($this->flatNumber, $this->name),
            BookingInformation::from(
                DateTimeImmutable::createFromMutable($this->startDate),
                DateTimeImmutable::createFromMutable($this->endDate),
                $this->quantity
            ),
            BookingType::from($this->type),
            BookingStatus::from($this->status),
            BookingAuditInformation::from(
                DateTimeImmutable::createFromMutable($this->createdDate),
                $this->updatedDate ? DateTimeImmutable::createFromMutable($this->endDate) : null,
                $this->feedback
            ),
            PaymentStatus::from($this->paymentStatus),
            BookingId::fromId($this->id)
        );
    }
}