<?php

declare(strict_types=1);

namespace App\Infrastructure\Services;

use App\Domain\Booking\Booking;
use App\Domain\Booking\Bookings;
use App\Domain\Booking\ValueObject\BookingId;
use App\Infrastructure\Doctrine\Repository\BookingReadRepository;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

final class FetchBooking
{
    private BookingReadRepository $repository;

    public function __construct(BookingReadRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getBookings(): Bookings
    {
        return $this->repository->findBookings(2021);
    }

    /**
     * @throws ResourceNotFoundException
     */
    public function getBooking(BookingId $bookingId): Booking
    {
        return $this->repository->findBookingById($bookingId);
    }
}