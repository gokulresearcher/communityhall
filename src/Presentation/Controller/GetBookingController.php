<?php

declare(strict_types=1);

namespace App\Presentation\Controller;

use App\Application\Query\GetBooking;
use App\Application\Query\GetBookingRequest;
use App\Application\Query\GetBookings;
use App\Application\Query\GetBookingsRequest;
use App\Application\Query\QueryBus;
use App\Domain\Booking\BookingNotFound;
use App\Domain\Booking\ValueObject\BookingId;
use App\Presentation\Resource\Booking\GetBookingResource;
use App\Presentation\Resource\Booking\GetBookingsResource;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class GetBookingController extends AbstractController
{
    private QueryBus $queryBus;

    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @Route("bookings", name="get_bookings_collection", methods={"GET"})
     */
    public function getBookingsAction(): Response
    {
        $bookings = $this->queryBus->query(new GetBookingsRequest());

        return GetBookingsResource::toJson($bookings);
    }

    /**
     * @Route("bookings/{id}", name="get_bookings_id", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function getBookingAction(int $id): Response
    {
        try {
            $bookingId = BookingId::fromId($id);
            $booking = $this->queryBus->query(new GetBookingRequest($bookingId));
        } catch (BookingNotFound $exception) {
            throw new NotFoundHttpException($exception->getMessage(),null, Response::HTTP_NOT_FOUND);
        }

        return GetBookingResource::toJson($booking);
    }
}