<?php

declare(strict_types=1);

namespace App\Presentation\Resource\Booking;

use App\Domain\Booking\Booking;
use Symfony\Component\HttpFoundation\JsonResponse;

final class GetBookingResource
{
    public static function toJson(?Booking $booking): JsonResponse
    {
        $data = [];

        if ($booking) {
            $data = [
                "id" => $booking->getBookingId()->getId(),
                "flatNumber" => $booking->getBookingUser()->getFlatNumber(),
                "name" => $booking->getBookingUser()->getName(),
                "bookedDate" => $booking->getBookingAuditInformation()->getCreatedDate()->format('Y-m-d'),
                "bookingDate" => $booking->getBookingInformation()->getStartDate()->format('Y-m-d'),
                "status" => $booking->getBookingStatus()->getStatus()
            ];
        }

        return new JsonResponse($data);
    }
}