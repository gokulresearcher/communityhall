<?php

declare(strict_types=1);

namespace App\Presentation\Resource\Booking;

use App\Domain\Booking\Bookings;
use Symfony\Component\HttpFoundation\JsonResponse;

final class GetBookingsResource
{
    public static function toJson(Bookings $bookings): JsonResponse
    {
        $data = [];

        foreach ($bookings->getBookings() as $booking) {
            $data[] = [
                "id" => $booking->getBookingId()->getId(),
                "flatNumber" => $booking->getBookingUser()->getFlatNumber(),
                "name" => $booking->getBookingUser()->getName(),
                "bookedDate" => $booking->getBookingAuditInformation()->getCreatedDate()->format('Y-m-d'),
                "bookingDate" => $booking->getBookingInformation()->getStartDate()->format('Y-m-d'),
                "status" => $booking->getBookingStatus()->getStatus()
            ];
        }

        return new JsonResponse($data);
    }
}