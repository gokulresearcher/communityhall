<?php

declare(strict_types=1);

namespace App\Domain\Payment\ValueObject;

use \DateTimeImmutable;

class AmountPaid
{
    private float $amountPaid;
    private DateTimeImmutable $amountPaidDate;
    private PaymentMode $amountPaidMode;

    private function __construct(float $amountPaid, DateTimeImmutable  $amountPaidDate, PaymentMode  $mode)
    {
        $this->amountPaid = $amountPaid;
        $this->amountPaidDate = $amountPaidDate;
        $this->amountPaidMode = $mode;
    }
    public static function from(float $amountPaid, DateTimeImmutable  $amountPaidDate, PaymentMode  $mode): self
    {
        return new self($amountPaid, $amountPaidDate, $mode);
    }

    public function getAmountPaid(): float
    {
        return $this->amountPaid;
    }

    public function getAmountPaidDate(): DateTimeImmutable
    {
        return $this->amountPaidDate;
    }

    public function getAmountPaidMode(): PaymentMode
    {
        return $this->amountPaidMode;
    }


}