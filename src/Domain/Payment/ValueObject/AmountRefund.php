<?php

declare(strict_types=1);

namespace App\Domain\Payment\ValueObject;

use \DateTimeImmutable;

class AmountRefund
{
    private float $amountRefund;
    private DateTimeImmutable $amountRefundDate;
    private PaymentMode $amountRefundMode;

    private function __construct(float $amountRefund, DateTimeImmutable  $amountRefundDate, PaymentMode  $mode)
    {
        $this->amountRefund = $amountRefund;
        $this->amountRefundDate = $amountRefundDate;
        $this->amountRefundMode = $mode;
    }
    public static function from(float $amountRefund, DateTimeImmutable  $amountRefundDate, PaymentMode  $mode): self
    {
        return new self($amountRefund, $amountRefundDate, $mode);
    }

    public function getAmountRefund(): float
    {
        return $this->amountRefund;
    }

    public function getAmountRefundDate(): DateTimeImmutable
    {
        return $this->amountRefundDate;
    }

    public function getAmountRefundMode(): PaymentMode
    {
        return $this->amountRefundMode;
    }


}