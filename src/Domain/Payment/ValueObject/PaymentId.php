<?php

declare(strict_types=1);

namespace App\Domain\Payment\ValueObject;


final class PaymentId
{
    private $id;

    private function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromId(int $id)
    {
        return new self($id);
    }

    public function getId(): int
    {
        return $this->id;
    }
}