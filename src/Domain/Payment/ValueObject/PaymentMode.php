<?php

declare(strict_types=1);

namespace App\Domain\Payment\ValueObject;

use Webmozart\Assert\Assert;

final class PaymentMode
{
    const CASH = 'cash';
    const ONLINE = 'online';

    const ALLOWED_MODE = [
        self::CASH,
        self::ONLINE,
    ];

    private string $paymentMode;

    private function __construct(string $paymentMode)
    {
        Assert::oneOf($paymentMode,self::ALLOWED_MODE, 'Not a valid payment mode : ' . $paymentMode);
        $this->paymentMode = $paymentMode;
    }

    public function getStatus(): string
    {
        return $this->paymentMode;
    }

    public static function cash(): self
    {
        return new self(self::CASH);
    }

    public static function online(): self
    {
        return new self(self::ONLINE);
    }
}