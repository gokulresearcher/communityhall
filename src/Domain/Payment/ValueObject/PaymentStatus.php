<?php

declare(strict_types=1);

namespace App\Domain\Payment\ValueObject;

use Webmozart\Assert\Assert;

final class PaymentStatus
{
    const PENDING = 'pending';
    const RECEIVED = 'received';

    const ALLOWED_STATUS = [
        self::PENDING,
        self::RECEIVED,
    ];

    private string $status;

    private function __construct(string $status)
    {
        Assert::oneOf($status,self::ALLOWED_STATUS, 'Not a valid payment status : ' . $status);
        $this->status = $status;
    }

    public static function from(string $status): self
    {
        return new self($status);
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public static function pending(): self
    {
        return new self(self::PENDING);
    }

    public static function received(): self
    {
        return new self(self::RECEIVED);
    }
}