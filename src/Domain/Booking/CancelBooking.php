<?php

declare(strict_types=1);

namespace App\Domain\Booking;

use App\Domain\Booking\ValueObject\BookingId;

interface CancelBooking
{
    public function cancelBooking(BookingId $bookingId): void;
}