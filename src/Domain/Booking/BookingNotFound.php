<?php

declare(strict_types=1);

namespace App\Domain\Booking;

use App\Domain\Booking\ValueObject\BookingId;
use \DomainException;

final class BookingNotFound extends DomainException
{
    public static function becauseTheBookingIdDoesNotExists(BookingId $bookingId): self
    {
        return new self(
            sprintf('Sorry the booking id %s is not found', $bookingId->getId())
        );
    }
}