<?php

declare(strict_types=1);

namespace App\Domain\Booking;

use App\Domain\Booking\ValueObject\BookingId;

interface SaveBooking
{
    public function saveBooking(Booking $booking): BookingId;
}