<?php

declare(strict_types=1);

namespace App\Domain\Booking;

use App\Domain\Booking\ValueObject\BookingAuditInformation;
use App\Domain\Booking\ValueObject\BookingId;
use App\Domain\Booking\ValueObject\BookingInformation;
use App\Domain\Booking\ValueObject\BookingStatus;
use App\Domain\Booking\ValueObject\BookingType;
use App\Domain\Booking\ValueObject\BookingUser;
use App\Domain\Payment\ValueObject\PaymentStatus;

final class Booking
{
    private ?BookingId $bookingId;
    private BookingUser $bookingUser;
    private BookingInformation $bookingInformation;
    private BookingType $bookingType;
    private BookingStatus $bookingStatus;
    private BookingAuditInformation $bookingAuditInformation;
    private PaymentStatus $paymentStatus;

    public function __construct(
        BookingUser $bookingUser,
        BookingInformation $bookingInformation,
        BookingType $bookingType,
        BookingStatus $bookingStatus,
        BookingAuditInformation $bookingAuditInformation,
        PaymentStatus $paymentStatus,
        ?BookingId $bookingId
    ) {
        $this->bookingUser = $bookingUser;
        $this->bookingInformation = $bookingInformation;
        $this->bookingStatus = $bookingStatus;
        $this->bookingAuditInformation = $bookingAuditInformation;
        $this->paymentStatus = $paymentStatus;
        $this->bookingId = $bookingId;
    }

    public static function populateData(
        BookingUser $bookingUser,
        BookingInformation $bookingInformation,
        BookingType $bookingType,
        BookingStatus $bookingStatus,
        BookingAuditInformation $bookingAuditInformation,
        PaymentStatus $paymentStatus,
        BookingId $bookingId
    ): self {
        return new self(
            $bookingUser,
            $bookingInformation,
            $bookingType,
            $bookingStatus,
            $bookingAuditInformation,
            $paymentStatus,
            $bookingId
        );
    }

    public function getBookingId(): ?BookingId
    {
        return $this->bookingId;
    }

    public function getBookingUser(): BookingUser
    {
        return $this->bookingUser;
    }

    public function getBookingInformation(): BookingInformation
    {
        return $this->bookingInformation;
    }

    public function getBookingType(): BookingType
    {
        return $this->bookingType;
    }

    public function getBookingStatus(): BookingStatus
    {
        return $this->bookingStatus;
    }

    public function getBookingAuditInformation(): BookingAuditInformation
    {
        return $this->bookingAuditInformation;
    }

    public function getPaymentStatus(): PaymentStatus
    {
        return $this->paymentStatus;
    }
}