<?php

declare(strict_types=1);

namespace App\Domain\Booking;

use Webmozart\Assert\Assert;

final class Bookings
{
    private array $bookings;

    private function __construct(array $bookings)
    {
        Assert::allIsInstanceOf($bookings, Booking::class);
        $this->bookings = $bookings;
    }

    public static function fromArray(array $bookings): self
    {
        return new self($bookings);
    }

    public static function empty(): self
    {
        return new self([]);
    }

    public function getBookings(): array
    {
        return $this->bookings;
    }
}