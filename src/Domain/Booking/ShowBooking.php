<?php

declare(strict_types=1);

namespace App\Domain\Booking;

use App\Domain\Booking\ValueObject\BookingId;
use App\Domain\Booking\ValueObject\BookingStatus;

interface ShowBooking
{
    public function findBookings(int $forYear): Bookings;

    public function findBookingById(BookingId $bookingId): Booking;

    public function checkAvailability(\DateTimeImmutable $date): BookingStatus;
}