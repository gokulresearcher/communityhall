<?php

declare(strict_types=1);

namespace App\Domain\Booking\ValueObject;

use Webmozart\Assert\Assert;

final class BookingType
{
    const HALL = 'hall';
    const CHAIR = 'hall';
    const TABLE = 'table';

    const ALLOWED_TYPES = [
        self::HALL,
        self::CHAIR,
        self::TABLE,
    ];

    private string $type;

    private function __construct(string $type)
    {
        Assert::oneOf($type,self::ALLOWED_TYPES, 'Not a valid booking type : ' . $type);
        $this->type = $type;
    }
    public static function from(string $type): self
    {
        return new self($type);
    }

    public function getType(): string
    {
        return $this->type;
    }

    public static function hall(): self
    {
        return new self(self::HALL);
    }

    public static function chair(): self
    {
        return new self(self::CHAIR);
    }

    public static function table(): self
    {
        return new self(self::TABLE);
    }
}