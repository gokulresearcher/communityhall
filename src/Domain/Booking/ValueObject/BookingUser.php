<?php

declare(strict_types=1);

namespace App\Domain\Booking\ValueObject;

final class BookingUser
{
    private string $flatNumber;

    private string $name;

    private function __construct(string $flatNumber, string $name)
    {
        $this->flatNumber = $flatNumber;
        $this->name = $name;
    }

    public static function from(string $flatNumber, string $name)
    {
        return new self($flatNumber, $name);
    }

    public function getFlatNumber(): string
    {
        return $this->flatNumber;
    }

    public function getName(): string
    {
        return $this->name;
    }
}