<?php

declare(strict_types=1);

namespace App\Domain\Booking\ValueObject;

final class BookingId
{
    private int $id;

    private function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromId(int $id)
    {
        return new self($id);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return strval($this->id);
    }
}