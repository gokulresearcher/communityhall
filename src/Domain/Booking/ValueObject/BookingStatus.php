<?php

declare(strict_types=1);

namespace App\Domain\Booking\ValueObject;

use Webmozart\Assert\Assert;

final class BookingStatus
{
    const APPROVED = 'approved';
    const PENDING = 'pending';
    const AVAILABLE = 'available';
    const RESERVED = 'reserved';
    const CANCEL = 'cancel';

    const ALLOWED_STATUS = [
        self::PENDING,
        self::APPROVED,
        self::AVAILABLE,
        self::RESERVED,
        self::CANCEL,
    ];

    private string $status;

    private function __construct(string $status)
    {
        Assert::oneOf($status,self::ALLOWED_STATUS, 'Not a valid booking status : ' . $status);
        $this->status = $status;
    }

    public static function from(string $status): self
    {
        return new self($status);
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public static function pending(): self
    {
        return new self(self::PENDING);
    }

    public static function approved(): self
    {
        return new self(self::APPROVED);
    }

    public static function available(): self
    {
        return new self(self::AVAILABLE);
    }

    public static function reserved(): self
    {
        return new self(self::RESERVED);
    }

    public static function cancel(): self
    {
        return new self(self::CANCEL);
    }
}