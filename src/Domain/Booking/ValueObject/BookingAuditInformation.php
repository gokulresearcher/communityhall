<?php

declare(strict_types=1);

namespace App\Domain\Booking\ValueObject;

use \DateTimeImmutable;

final class BookingAuditInformation
{
    private DateTimeImmutable $createdDate;

    private ?DateTimeImmutable $updatedDate;

    private ?string $feedback;

    private function __construct(
        DateTimeImmutable $createdDate,
        ?DateTimeImmutable $updatedDate,
        ?string $feedback
    ) {
        $this->createdDate = $createdDate;
        $this->updatedDate = $updatedDate;
        $this->feedback = $feedback;
    }

    public static function from(
        DateTimeImmutable $createdDate,
        ?DateTimeImmutable $updatedDate,
        ?string $feedback
    ): self {
        return new self($createdDate, $updatedDate, $feedback);
    }

    public function getCreatedDate(): DateTimeImmutable
    {
        return $this->createdDate;
    }

    public function getUpdatedDate(): ?DateTimeImmutable
    {
        return $this->updatedDate;
    }

    public function getFeedback(): ?string
    {
        return $this->feedback;
    }
}