<?php

declare(strict_types=1);

namespace App\Domain\Booking\ValueObject;

use \DateTimeImmutable;

final class BookingInformation
{
    private DateTimeImmutable $startDate;

    private DateTimeImmutable $endDate;

    private int $quantity;

    private function __construct(
        DateTimeImmutable $startDate,
        DateTimeImmutable $endDate,
        int $quantity
    ) {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->quantity = $quantity;
    }

    public static function from(
        DateTimeImmutable $startDate,
        DateTimeImmutable $endDate,
        int $quantity
    ): self {
        return new self($startDate, $endDate, $quantity);
    }

    public function getStartDate(): DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getEndDate(): DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }
}