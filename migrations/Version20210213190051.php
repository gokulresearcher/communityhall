<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210213190051 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE booking (id INT AUTO_INCREMENT NOT NULL, flat_number VARCHAR(3) NOT NULL, name VARCHAR(100) NOT NULL, type ENUM(\'hall\', \'chair\', \'table\'), reason LONGTEXT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, quantity SMALLINT NOT NULL, status ENUM(\'pending\', \'approved\'), payment_status ENUM(\'pending\', \'approved\'), created_date DATETIME NOT NULL, updated_date DATETIME DEFAULT NULL, feedback LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment (id INT AUTO_INCREMENT NOT NULL, booking_id INT DEFAULT NULL, amount_paid_for_house_cleaning DOUBLE PRECISION NOT NULL, amount_pending DOUBLE PRECISION NOT NULL, amount_paid DOUBLE PRECISION NOT NULL, amount_paidd_date DATETIME NOT NULL, amount_paid_mode ENUM(\'cash\', \'googlepay\'), amount_refund DOUBLE PRECISION NOT NULL, amount_refunded_date DATETIME NOT NULL, amount_refund_mode ENUM(\'cash\', \'googlepay\'), created_date DATETIME NOT NULL, updated_date DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_6D28840D3301C60 (booking_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D3301C60 FOREIGN KEY (booking_id) REFERENCES booking (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840D3301C60');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE payment');
    }
}
